# Generated by Django 4.1.13 on 2024-04-12 11:56

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("webservice", "0114_auto_20240213_1159"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="maplayer",
            options={"verbose_name": "Kaartlaag",
                     "verbose_name_plural": "Kaartlagen"},
        ),
    ]
