# Generated by Django 4.1.9 on 2023-11-02 22:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("webservice", "0099_alter_viewer_type"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="layer",
            options={
                "ordering": ["layer_type__ordering", "ordering", "title"],
                "verbose_name": "Kaartlaag",
                "verbose_name_plural": "Kaartlagen",
            },
        ),
        migrations.AddField(
            model_name="layer",
            name="templated_properties",
            field=models.JSONField(
                blank=True,
                default=dict,
                help_text="Velden die samengesteld worden vanuit een template",
                null=True,
                verbose_name="Templatevelden",
            ),
        ),
    ]
