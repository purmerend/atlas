# Generated by Django 4.1.9 on 2023-11-12 19:21

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("webservice", "0101_layer_meta_contact_layer_meta_lineage_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="layer",
            name="meta_lineage",
            field=models.TextField(
                blank=True,
                help_text="Beschrijft de herkomst van de dataset. Het is mogelijk om tekst op te maken met Markdown in dit veld",
                null=True,
                verbose_name="Bron",
            ),
        )
    ]
