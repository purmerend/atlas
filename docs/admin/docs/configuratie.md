Hier kunnen de globale instellingen gedaan worden die in Atlas getoond worden.  

### Organisatie  

- **Naam Organisatie**  
De naam van de organisatie die getoond moet worden.

- **Huidige logo**  
Deze afbeelding komt linksboven in Atlasscherm te staan.  

- **Favicon URL**  
Verwijzing naar een icon. Dit wordt getoond in de tabbladen van de browser.  
Favicons kunnen zijn PNG, SVG, JPG of ICO, waarbij SVG formaat vector georiënteerd is.  
Groote kan varieren van 16x16, 32x32, 48x48, 96x96 tot 144x144  

### Kaartconfiguratie  

- **Centrum X-coördinaat**  
Het centrum X-coördinaat van de opstartpositie.  

- **Centrum Y-coördinaat**  
Het centrum Y-coördinaat van de opstartpositie. 

- **Zoomniveau**  
Het zoomniveau van de opstartpositie.

- **Doorzoek deze gemeentes**  
Een komma-gescheiden lijst van gemeenten om adressen in te zoeken (voor auto-aanvulfunctionaliteit).  

- **Standaard kaartgebied**  
Configureer een gebied dat standaard uitgelicht wordt op de kaart.  

### Matomo  

- **Matomo Site Url**  
- **Matomo Site Id**  
[Matomo](https://github.com/matomo-org/matomo) kan gebruikt wordenom statistieken bij te houden. Vul in deze velden de benodigde informatie in.  

### Features  

- **Portaal functionaliteit**  
Zet de portaalfunctionaliteit aan of uit als startscherm. In het portaal kunnen bijvoorbeeld kaarten worden aangeboden.  
- **Print functionaliteit** 
Geef de mogelijkheid om een PDF bestand van het zichtbare scherm te maken in verschillende resoluties.  
- **Tekenfunctionaliteit**  
Geef de mogelijkheid om in de zichtbare kaart te tekenen en dit op te slaan als URL.    
- **Zet oude beheerpaneel uit**  
Tijdelijke feature in de overgangsperiode van de oude naar de nieuwe admin module.  
