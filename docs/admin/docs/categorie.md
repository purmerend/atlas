<!--

<div id="categorieen">
<h2>CATEGORIEËN</h2>
</div>
---
-->

Categorieën zijn de hoofdonderwerpen zoals die links in het hoofdscherm van Atlas worden getoond. Het openklikken van een hoofdonderwerp/categorie
zorgt ervoor dat eronder de verschillende kaartlagen van die categorie in de legenda worden getoond.
Bij het toevoegen van een kaartlaag wordt aangegeven onder welke categorie deze valt.

<img src="../images/categorieen.png" alt="categorieën" width="400"/>

### Categorieën beheren

Binnen de adminmodule van Atlas kunnen categorieën beheerd worden

<img src="../images/categorie.png" alt="categorieën" width="400"/>

Klik op Acties om Bestaande categoriegegevens [in te lezen of te exporteren](dashboard.md#importeren-en-exporteren-van-configuratiegegevens){target="\_blank"}.  
Bestaande categorieën kunnen automatisch alfabetisch of handmatig gesorteerd worden. Klik op Sortering om in het sorteringscherm te komen.
Klik in de lijst met categorieën op een categorie en sleep deze naar de gewenste plek.  
Om de categorieë automatisch te sorteren, klik op de knop Sorteer alfabetisch.

!!! warning "Waarschuwing"

    Bij het klikken op de knop Sorteer alfabetisch, wordt de sortering direct aangepast. Dit is niet terug te draaien.
    Het kan dus handig zijn om bij aanpassingen eerst een JSON export van de categoriegegevens te maken.

De volgende velden moeten worden ingevuld bij het toevoegen van een categorie:

- **Titel:** De naam zoals die in het viewer scherm van Atlas komt te staan
- **Kort kenmerk:** Dit moet een unieke sleutel zijn. spaties zijn niet toegestaan.
