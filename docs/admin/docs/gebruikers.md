Gebruikers binnen Atlas worden automatisch aangemaakt op het moment dat ze inloggen. Uiteraard onder voorwaarde dat een gebruiker al bestaat binnen het lokale netwerk op een LDAP server of Active Directory. Er is dus geen optie om gebruikers aan te maken in de Adminmodule van Atlas.  
De gebruikergegevens kunnen wel bewerkt worden, klik daarvoor op de Wijzig knop in de lijstweergave.

###Gebruiker wijzigen

- **Algemene gegevens**  
  -- **Gebruikersnaam**  
  -- **Externe ID**

- **Persoonlijke gegevens**  
  -- **Volledige naam**  
  -- **E-mailadres**

- **Rechten**  
  -- **Actief:** Vink deze optie uit wanneer deze gebruiker niet meer mag inloggen maar niet verwijderd hoeft te worden.  
  -- **Beheerder:** Vink deze optie aan wanneer de gebruiker alle administratieve rechten moet hebben.

- **Groepen**  
  -- **Groepen:** Selecteer hier de groepen waar de gebruiker onderdeel van moet zijn.

- **Belangrijke datums**  
  -- **Datum toegetreden**  
  -- **Laatste aanmelding**
