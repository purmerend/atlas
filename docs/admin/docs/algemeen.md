Onder de knop Algemene gegevens staat de informatie over de huidige installatie.

###Organisatie
Hier staat de naam van de organisatie zoals die binnen [Configuratie](configuratie.md) ingesteld is.

###Versie
Hier staat het versienummer van de geïnstalleerde Atlasversie.

###Omgeving
Hier staat de omgeving waarmee gewerkt wordt, bijvoorbeeld Ontwikkel, Test, Acceptatie of Productie.
