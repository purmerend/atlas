Kaarten zijn verzamelingen kaartlagen die samen over een bepaald onderwerp gaan. Om een kaart samen te stellen worden kaartlagen geselecteerd die bij die kaart horen.
Een kaart verschijnt niet in het Atlas hoofdscherm scherm. Bij het aanmaken van een kaart wordt als het ware een aparte instantie van Atlas gecreëerd met een beperkter aantal kaartlagen en beperktere functionaliteit. Wanneer bijvoorbeeld een kaart 'hondenbeleid' is aangemaakt waarin de kaartlagen 'hondenuitlaatplekken' en 'hondenbakken' zitten, dan kan de url om de kaart op te vragen er bijvoorbeeld zo uitzien: https://mijngemeentewebsite.nl/atlas/maps/hondenbeleid

<img src="../images/hondenbeleid.png" alt="hondenbeleid" width="500"/>

Om een kaart aan te maken klik je in het Kaarten menu op de knop Nieuwe kaart.
Vul de twee velden in. De titel mag uit meerdere woorden bestaan. Het veld Kort kenmerk mag geen spaties bevatten, wel (liggende)streepjes. Dit veld moet ook een unieke waarde hebben.
Klik op Opslaan en openen wanneer beide velden zijn ingevuld.

<img src="../images/nieuwe_kaart.png" alt="Kaart Toevoegen" width="800"/>

In het kaartmenu kunnen de verschillende opties voor een kaart geconfigureerd worden.  
Een optie die aangeklikt wordt, is direct in het voorbeeldscherm te zien.
Sommige opties hebben een submenu met keuzes, dit wordt zichtbaar nadat de optie is aangevinkt.

### Kaart lagen

<img src="../images/kaartmenu.png" alt="Kaart Toevoegen" width="500"/>

- **Titel**. Deze kan eventueel nog worden aangepast. Let hierbij op dat de URL wel de originele naam zal bevatten.
- [**Lagen**](#kaart-lagen). Hier kunnen de beschikbare kaartlagen worden gekozen en geconfigureerd.
- **Toon** zoekbalk  
  De zoekbalk zoals die linksboven in het Atlas scherm getoond wordt.  
  <img src="../images/zoekbalk.png" alt="zoekbalk" width="400"/>
- **Toon** dataweergave  
  <img src="../images/dataweergave.png" alt="zoekbalk" width="50"/>
- **Selecteer** gebied  
  <img src="../images/selecteer_gebied.png" alt="zoekbalk" width="50"/>
- **Opmeten**  
  <img src="../images/opmeten.png" alt="zoekbalk" width="50"/>
- **Meer opties**  
  <img src="../images/meer_opties.png" alt="zoekbalk" width="50"/>
- **GPS knop**  
  <img src="../images/gps_knop.png" alt="zoekbalk" width="50"/>
- **Zoomfunctie**  
  <img src="../images/zoomfunctie.png" alt="zoekbalk" width="50"/>
- **Toon schaal**  
  <img src="../images/toon_schaal.png" alt="zoekbalk" width="100"/>
- **Prikker bij klik**  
  Toont het 'prikker' symbool wanneer ergens op de kaart geklikt wordt.  
  <img src="../images/prikker_bij_klik.png" alt="zoekbalk" width="50"/>
- **Basislagen**  
  <img src="../images/basislagen.png" alt="zoekbalk" width="50"/>
- **Lagenlijst**  
  Bij lagenlijst kan na aanvinken gekozen worden om de zoekbalk lagenlijst te verbergen en om een versimpelde weergave van de lagenlijst te laten zien.  
  <img src="../images/lagenlijst.png" alt="zoekbalk" width="50"/>
- **Legenda**  
  <img src="../images/legenda.png" alt="zoekbalk" width="50"/>
- **Lijstweergave**  
  De lijstweergave kan na aanvinken in het submenu verder geconfigureerd worden.  
  <img src="../images/lijstweergave.png" alt="zoekbalk" width="100"/>
- **Filters**  
  De filterfunctie kan na aanvinken in het submenu verder geconfigureerd worden.  
  <img src="../images/filters.png" alt="zoekbalk" width="100"/>

Klik op opslaan om de gemaakte keuzes te bewaren.
