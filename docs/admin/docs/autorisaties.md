Binnen autorisaties kan geconfigureerd worden welke data van een API verbinding worden doorgegeven aan een groep.
Binnen het veld Resource kan gebruikt gemaakt worden van [regular expressions](https://en.wikipedia.org/wiki/Regular_expression).  
Een gebruikte bron binnen Autorisaties dient te verwijzen naar een API.
