---
### GROEPEN
---

In het Atlas Groepen scherm kunnen groepen worden aangemaakt. Gebruikers kunnen in het [gebruikersscherm](gebruikers.md) groepen krijgen toegewezen.
