Gebruikte afkortingen
=====================

In gemeenteland worden veel afkortingen gebruikt. Het is niet vanzelfsprekend dat iedereen weet waar deze afkortingen voor staan. Daarom hier een opsomming van de afkortingen die binnen Atlas gebruikt worden. Zie ook [Gebruikte termen](./gebruiktetermen.md#termen-en-naamgeving)

## AFWC  
Amsterdamse Federatie van Woningcorporaties
## APV
Algemene Plaatselijke Verordening
## AVG
Algemene Verordening Gegevensbescherming
## BAG
Basisregistratie Adressen en Gebouwen
## BGT
Basisregistratie Grootschalige Topografie  
De BGT is gedetailleerder dan de BRT (Basisregistratie Topologie) en heeft in tegenstelling tot de BRT meerdere bronhouders.
Gemeenten, provincies en waterschappen maken de BGT samen met het ministerie van Landbouw, Natuur en Voedselkwaliteit (LNV), Defensie en Infrastructuur en Waterstaat (IenW). Iedere bronhouder is verantwoordelijk voor zijn eigen stukje van de digitale kaart. De bronhouders zijn georganiseerd in de stichting Samenwerkingsverband Bronhouders voor de BGT (SVB-BGT).
## BRT
Basisregistratie Topologie  
De BRT bestaat uit digitale topografische bestanden op verschillende schaalniveaus
(1:10.000, 1:50.000, 1:100.000, 1:250.000, 1:500.000, en 1:1.000.000). 
Het Kadaster is houder van de BRT.
## BRK
Basisregistratie Kadaster
## BRP
Basisregistratie Personen (Opvolger GBA - gemeentelijke basisadministratie persoonsgegevens)
## HHNK
Hoogheemraadschap Hollands Noorderkwartier
## NGR
Nationaal Georegister
## PDOK 
Publieke Dienstverlening Op de Kaart
## PWN 
Provinciaal Waterleidingbedrijf Noord-Holland
## RIVM 
Rijksinstituut voor Volksgezondheid en Milieu
## WOZ 
Waardering Ontroerende Zaken


Zie ook: [Gebruikte termen](./gebruiktetermen.md)
