# Wat is Atlas?

Atlas is een geoportaal met een gebruiksvriendelijke interface waarmee op simpele wijze lagen getoond kunnen worden vanuit een WMS, WFS, WMTS en vector-tiles (MVT) server.
Atlas is een [open source applicatie](https://gitlab.com/purmerend/atlas) die ontwikkeld is door [Datalab Purmerend](https://datalab.purmerend.nl/), dat onderdeel is van de gemeente Purmerend, en [Delta10](https://www.delta10.nl).
Atlas wordt door verschillende Nederlandse gemeenten gebruikt als (geo)informatiebron voor inwoners en de interne organisatie.

<img src="https://gitlab.com/purmerend/atlas/uploads/e549ad00397d4f0f593f703ee12ceb9b/image.png" alt="Screenshot of Atlas" width="500"/>
