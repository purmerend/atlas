Termen en naamgeving
====================

Binnen Atlas worden verschillende termen gebruikt om onderdelen en functies te benoemen of uit te leggen.
Hieronder volgt een verklarende woordenlijst van deze termen. Zie ook [Gebruikte afkortingen](./afkortingen.md)

## *Kaartscherm*
   * Het scherm dat getoond wordt wanneer Atlas opgestart is. Dit is het scherm waarin de topografische (basis)kaart getoond wordt van het gebied waarmee gewerkt wordt.

## *Kaarten*
   * Kaarten zijn samengestelde kaartlagen. De kaartlagen die samen een kaart vormen hebben een thema. Zo kan een kaart 'Erfgoed' kaartlagen bevatten over monumenten, historische bebouwing, cultuurlandschap en andere kaartlagen die onder het thema erfgoed vallen.

## *Kaartlaag*
   * Een kaartlaag bevat informatie over één onderwerp. Meerdere kaartlagen vallen onder een [categorie](#categorie).   
 
## *Kaartlagenmenu*
   * Het menu waarin alle beschikbare kaartlagen getoond worden wanneer linkonder in het kaartscherm op het [*'Alle lagen' symbool*](./help.md#keuze-van-de-zichtbare-kaartlagen) geklikt wordt.
   
## *Meetfunctie*
   * Linksboven in het kaartscherm staat een symbool met een lineaal. Hiermee wordt de [*meetfunctie*](./help.md#meten) geactiveerd. Atlas kan oppervlakte en afstand meten in meters.
 

## *Categorie*
   * [*Categorieën*](./help.md#keuze-van-de-zichtbare-kaartlagen) waarbinnen kaartlagen over een bepaald onderwerp vallen. Een kaartlaag 'Scholen' valt bijvoorbeeld onder de categorie 'Wonen en leefomgeving'.
   
## *Detailpaneel*
   * Wanneer een [(WFS)](https://nl.wikipedia.org/wiki/Web_Feature_Service) kaartlaag is geselecteerd en een object op deze kaartlaag wordt aangeklikt, dan verschijnt aan de linkerkant van het kaartscherm de [detailinformatie](./help.md#details-van-objecten-tonen) over dit object
   
## *Metadata*
   * Metadata geeft informatie over de data. Bijvoorbeeld wanneer deze voor het laatst is bijgewerkt, wie verantwoordelijk is voor de juiste gegevens en wat het doel van de kaartlaag is.  

## *Legenda*
   * De legenda geeft uitleg over de gebruikte symbolen en kleuren op de kaartlaag



Zie ook: [Gebruikte afkortingen](./afkortingen.md) 
