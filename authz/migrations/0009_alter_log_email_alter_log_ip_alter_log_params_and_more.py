# Generated by Django 4.1.9 on 2023-12-10 18:32

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("authz", "0008_rename_resources_log_resource"),
    ]

    operations = [
        migrations.AlterField(
            model_name="log",
            name="email",
            field=models.EmailField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="log",
            name="ip",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="log",
            name="params",
            field=models.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="log",
            name="resource",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="log",
            name="source",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="log",
            name="username",
            field=models.CharField(max_length=255),
        ),
    ]
